define(['angular', 'angular_ui_router'], function() {
  var module;
  module = angular.module('app.states.home', ['ui.router']);
  return module.config(function($stateProvider) {
    return $stateProvider.state("home", {
      templateUrl: "app/states/home/home",
      url: "/home",
      controller: "HomeCtrl"
    });
  });
});

//# sourceMappingURL=home-module.js.map
