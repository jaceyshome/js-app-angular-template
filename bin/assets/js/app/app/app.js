define(['angular', 'angular_ui_router', 'templates', 'app/states/home/home-ctrl'], function() {
  var module;
  module = angular.module('app', ['templates', 'ui.router', 'app.states.home']);
  module.config(function($locationProvider, $urlRouterProvider) {
    $locationProvider.html5Mode(true);
    return $urlRouterProvider.otherwise('/home');
  });
  module.controller('MainCtrl', function($scope) {
    return $scope.ready = true;
  });
  return module;
});

//# sourceMappingURL=app.js.map
