define(['angular'], function() {angular.module('templates', []).run([ '$templateCache', function($templateCache) {  'use strict';

  $templateCache.put('app/app',
    "<div data-ng-controller=MainCtrl><div data-ng-hide=true class=container><div class=loading></div></div><div data-ng-cloak=data-ng-cloak class=container><div data-ng-hide=ready><div class=loading></div></div><div data-ng-show=ready class=initialiseWrapper><div data-ui-view=data-ui-view></div></div></div></div>"
  );


  $templateCache.put('app/states/home/home',
    "<div class=home><p>this is home page</p></div>"
  );


  $templateCache.put('common/worldmap/main',
    "<div class=tntWorldMap><div id=container></div></div>"
  );
} ]);});