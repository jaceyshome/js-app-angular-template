requirejs.config({
  waitSeconds: 200,
  urlArgs: "bust=" + (new Date()).getTime(),
  paths: {
    jquery: "../lib/jquery/dist/jquery",
    bootstrap: "../lib/bootstrap/dist/js/bootstrap",
    angular: "../lib/angular/angular",
    angular_resource: "../lib/angular-resource/angular-resource",
    angular_ui_router: "../lib/angular-ui-router/release/angular-ui-router",
    angular_sanitize: "../lib/angular-sanitize/angular-sanitize",
    angular_animate: "../lib/angular-animate/angular-animate",
    xml2json: "../lib/x2js/xml2json",
    videojs: "../lib/video.js/dist/video-js/video.dev",
    bowser: "../lib/bowser/bowser",
    preload: "../lib/PreloadJS/lib/preloadjs-0.4.1.min",
    underscore: "../lib/underscore/underscore",
    raphael: "../lib/raphael/raphael",
    raphael_scale: "../lib/raphael_scale/index",
    eve: "../lib/eve/eve",
    jqplaceholder: "../lib/jquery-placeholder/jquery.placeholder"
  },
  shim: {
    angular: {
      deps: ['jquery'],
      exports: 'angular'
    },
    bootstrap: {
      deps: ['jquery'],
      exports: 'bootstrap'
    },
    angular_resource: {
      deps: ['angular'],
      exports: 'angular_resource'
    },
    angular_ui_router: {
      deps: ['angular'],
      exports: 'angular_ui_router'
    },
    angular_sanitize: {
      deps: ['angular'],
      exports: 'angular_sanitize'
    },
    angular_animate: {
      deps: ['angular'],
      exports: 'angular_animate'
    },
    raphael: {
      deps: ['jquery', 'angular', 'eve'],
      exports: 'raphael'
    },
    raphael_scale: {
      deps: ['jquery', 'angular', 'raphael', 'eve'],
      exports: 'raphael_scale'
    },
    jqplaceholder: {
      deps: ['jquery'],
      exports: 'jqplaceholder'
    }
  }
});

define(['angular', 'bowser', 'app/app'], function(angular, bowser) {
  return angular.element(document).ready(function() {
    return angular.bootstrap(document, ['app']);
  });
});

//# sourceMappingURL=main.js.map
