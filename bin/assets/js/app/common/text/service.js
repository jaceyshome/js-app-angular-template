define(['angular', 'xml2json'], function() {
  var module;
  module = angular.module('common.text.service', []);
  return module.factory('Text', function($http, $q) {
    var service;
    service = {};
    service.load = function(callback) {
      var deferred;
      deferred = $q.defer();
      $http.get('assets/data/text.xml', {
        transformResponse: function(data) {
          var json, x2js;
          x2js = new X2JS;
          return json = x2js.xml_str2json(data);
        }
      }).success(function(data, status) {
        var key, texts, value, _fn, _ref;
        texts = {};
        _ref = data.texts;
        _fn = function(key, value) {
          if (value.__cdata) {
            return texts[key] = value.__cdata;
          }
        };
        for (key in _ref) {
          value = _ref[key];
          _fn(key, value);
        }
        service.texts = texts;
        return deferred.resolve(texts);
      });
      if (callback) {
        deferred.promise.then(callback);
      }
      return deferred.promise;
    };
    service.getPulleyText = function(key) {
      return $(this.texts[key]).text();
    };
    return service;
  });
});

//# sourceMappingURL=service.js.map
