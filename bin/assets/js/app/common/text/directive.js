define(['angular'], function() {
  var module;
  module = angular.module('common.text.directive', []);
  return module.directive('tntText', function($compile, Text) {
    return {
      restrict: "A",
      scope: {
        tntText: "=",
        removeHtmlTags: "="
      },
      link: function($scope, element, attrs) {
        return $scope.$watch("tntText", function(val) {
          var content;
          if ((val != null) && Text.texts) {
            if (Text.texts[val]) {
              content = Text.texts[val];
              if ($scope.removeHtmlTags) {
                content = $(content).text();
              }
              element.html(content);
              return $compile(element.contents())($scope);
            } else {
              console.log("Error: No text with id '" + val + "'");
              return element.html(val);
            }
          }
        });
      }
    };
  });
});

//# sourceMappingURL=directive.js.map
