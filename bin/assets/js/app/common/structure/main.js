define(['angular'], function(angular, jsyaml) {
  var module;
  module = angular.module('common.structure', []);
  return module.factory('Structure', function($http, $q) {
    var service;
    service = {};
    service.load = function(callback) {
      var deferred;
      deferred = $q.defer();
      if (service.data != null) {
        deferred.resolve(service.data);
      } else {
        $http.get('assets/data/structure.json').success(function(data, status) {
          service.data = data;
          return deferred.resolve(data);
        });
      }
      if (callback) {
        deferred.promise.then(callback);
      }
      return deferred.promise;
    };
    return service;
  });
});

//# sourceMappingURL=main.js.map
