define(['angular', 'common/worldmap/directive', 'common/worldmap/service'], function() {
  var module;
  return module = angular.module('common.worldmap', ['templates', 'common.worldmap.service', 'common.worldmap.directive']);
});

//# sourceMappingURL=main.js.map
