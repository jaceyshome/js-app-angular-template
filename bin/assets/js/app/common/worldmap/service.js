define(['angular'], function(angular) {
  var module;
  module = angular.module('common.worldmap.service', []);
  return module.factory('WorldMapService', function($http, $q) {
    var service;
    service = {};
    service.data = null;
    service.init = function(callback) {
      var deferred;
      deferred = $q.defer();
      if (service.data) {
        if (callback) {
          return callback();
        }
      } else {
        $http.get('assets/data/worldmap_settings.json').success(function(result, status) {
          service.data = result;
          return deferred.resolve(result);
        });
        if (callback) {
          deferred.promise.then(callback);
        }
        return deferred.promise;
      }
    };
    return service;
  });
});

//# sourceMappingURL=service.js.map
