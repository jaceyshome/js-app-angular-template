define(['angular', 'raphael', 'raphael_scale'], function() {
  var module;
  module = angular.module('common.worldmap.directive', ['templates', 'common.worldmap.service']);
  return module.directive('tntWorldMap', function(WorldMapService) {
    return {
      restrict: "A",
      scope: {
        handleMouseClick: "=",
        handleMouseHover: "=",
        handleMouseOut: "="
      },
      templateUrl: "common/worldmap/main",
      link: function($scope, element, attrs) {
        var createMap, createMapPaths, defaultSettings, init, mapContainer, mapPaths, registerMapPathEventListeners, resizeMap;
        defaultSettings = {
          width: 207,
          height: 200,
          curMapX: 0,
          curMapY: 0
        };
        mapContainer = null;
        mapPaths = [];
        init = function() {
          $scope.data = WorldMapService.data;
          return createMap();
        };
        createMap = function() {
          mapContainer = ScaleRaphael('container', defaultSettings.width, defaultSettings.height);
          createMapPaths();
          return resizeMap();
        };
        createMapPaths = function() {
          var attributes, mapPath, opacity, path, _i, _len, _ref, _results;
          opacity = 0;
          attributes = {
            fill: '#d9d9d9',
            'fill-opacity': opacity,
            cursor: 'pointer',
            stroke: '#BF2400',
            'stroke-opacity': opacity,
            'stroke-width': 0.5,
            'stroke-linejoin': 'round'
          };
          _ref = $scope.data.paths;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            path = _ref[_i];
            mapPath = mapContainer.path(path.path);
            mapPath.attr(attributes);
            mapPath.data = {
              id: path.id
            };
            registerMapPathEventListeners(mapPath);
            _results.push(mapPaths.push(mapPath));
          }
          return _results;
        };
        registerMapPathEventListeners = function(path) {
          path.mouseover(function(e) {
            if (typeof $scope.handleMouseHover === 'function') {
              return $scope.handleMouseHover(path.data);
            }
          });
          path.mouseout(function(e) {
            if (typeof $scope.handleMouseOut === 'function') {
              return $scope.handleMouseOut(path.data);
            }
          });
          return path.mouseup(function(e) {
            if (typeof $scope.handleMouseClick === 'function') {
              return $scope.handleMouseClick(path.data);
            }
          });
        };
        resizeMap = function() {
          return void 0;
        };
        return WorldMapService.init(init);
      }
    };
  });
});

//# sourceMappingURL=directive.js.map
