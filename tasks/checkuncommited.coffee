module.exports = (grunt)->
  shell = require 'shelljs'
  chalk = require 'chalk'

  grunt.registerMultiTask 'checkuncommited', 'Check git for unmerged branches', ->
    done = @async()
    options = @options()
    result = shell.exec 'git diff --exit-code', {silent:true}
    if result.code isnt 0
      grunt.fail.warn("You have uncommited changes")
    done()