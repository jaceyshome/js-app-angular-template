module.exports = (grunt)->
  chalk = require 'chalk'
  grunt.registerMultiTask 'checkdata', 'Check grunt data to ensure parameters are not still default etc', ->
    done = @async()
    options = @options()
    parameters = options.parameters
    for parameter in parameters
      regex = new RegExp parameter.pattern
      matches = parameter.value.match regex
      unless matches?
        if parameter.fatal
          grunt.fail.fatal(parameter.error)
        else
          grunt.fail.warn(parameter.error)
    done()