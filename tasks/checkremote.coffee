module.exports = (grunt)->
  shell = require 'shelljs'
  chalk = require 'chalk'

  grunt.registerMultiTask 'checkremote', 'Check if git remote exists. Add and use addserver staging script if not already setup.', ->
    done = @async()
    options = @options()
    parameters = options.parameters
    branch = options.branch
    projectName = options.projectName
    result = shell.exec 'git remote', {silent:true}
    remoteExists = false
    if result.output.length > 0
      splitRemotes = result.output.split "\n"
      for remote in splitRemotes
        remote = remote.trim()
        unless remote is ""
          remoteExists = true if remote is branch
    console.log "remoteExists", remoteExists
    unless remoteExists
      result = shell.exec "git remote add #{branch} ssh://ec2-user@2and2.com.au/home/ec2-user/#{projectName}.staging.2and2.com.au.git"
    result = shell.exec "git remote show #{branch}", {silent:true}
    if result.output.indexOf("does not appear to be a git repository") > -1
      result = shell.exec "ssh -t -t ec2-user@2and2.com.au 'sh addserver.sh --url #{projectName}.staging.2and2.com.au --subfolder y --subbranch y --wildcard y'"
      result = shell.exec "git remote show #{branch}", {silent:true}
      if result.output.indexOf("does not appear to be a git repository") > -1
        grunt.fail.fatal "git remote #{branch} is not a git repository, and could not be automatically created"
    done()