module.exports = (grunt)->
  grunt.registerMultiTask 'copylibs', 'Copy requried libs in app/main.coffee from bower_component', ->
    bowerComponentPath = 'bower_components'
    libPath = "bin/assets/js/lib"
    options =
      encoding: 'utf8'
    fileString = grunt.file.read 'src/main.coffee'
    pathStrings = ((fileString.split('paths:')[1]).split('shim:')[0]).split(/\n/g)
    for pathString in pathStrings
      path = pathString.trim().split(":")[1]
      continue unless path
      toPath = eval(path.replace("../lib", libPath)) + '.js'
      fromPath = eval(path.replace("../lib", bowerComponentPath)) + '.js'
      grunt.file.copy fromPath, toPath, options
