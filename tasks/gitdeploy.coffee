module.exports = (grunt)->
  shell = require 'shelljs'
  chalk = require 'chalk'

  grunt.registerMultiTask 'gitdeploy', 'Deploy to remote git', ->
    done = @async()
    options = @options()
    parameters = options.parameters
    branch = options.branch
    remote = options.remote
    remoteBranch = options.remoteBranch
    remoteAddress = options.remoteAddress
    mergeBranch = options.mergeBranch
    finishBranch = options.finishBranch

    ## check local and origin branched exist

    # console.log "branch", branch
    result = shell.exec 'git branch -a', {silent:true}
    # console.log "result", result
    localExists = false
    originExists = false
    if result.output.length > 0
      splitBranches = result.output.split "\n"
      #console.log "splitBranches", splitBranches
      for compareBranch in splitBranches
        compareBranch = compareBranch.trim()
        #console.log "checking", branch
        unless compareBranch is ""
          # console.log "compareBranch", compareBranch
          localExists = true if compareBranch is branch
          originExists = true if compareBranch is "remotes/origin/#{branch}"
    # console.log "localExists", localExists
    # console.log "originExists", originExists
    if originExists
      result = shell.exec "git checkout #{branch}"
    else
      if localExists
        result = shell.exec "git checkout #{branch}"
      else
        result = shell.exec "git checkout -b #{branch}"
      result = shell.exec "git push origin #{branch} -u"

    ## merge in from branch
      
    result = shell.exec "git pull"
    result = shell.exec "git merge #{mergeBranch}"
    result = shell.exec "git push"


    ## check remote exists and add if not

    result = shell.exec 'git remote', {silent:true}
    # console.log "result", result
    # return done()
    remoteExists = false
    if result.output.length > 0
      splitRemotes = result.output.split "\n"
      #console.log "splitBranches", splitBranches
      for checkRemote in splitRemotes
        checkRemote = checkRemote.trim()
        #console.log "checking", branch
        unless checkRemote is ""
          #console.log "remote", remote
          remoteExists = true if checkRemote is remote
    #console.log "remoteExists", remoteExists
    unless remoteExists
      result = shell.exec "git remote add #{remote} #{remoteAddress}"
    result = shell.exec "git remote show #{remote}", {silent:true}
    #console.log "result", result
    if result.output.indexOf("does not appear to be a git repository") > -1
      #console.log 
      grunt.fail.fatal "git remote #{remote} is not a git repository"


    ## push to remote

    result = shell.exec "git push #{remote} #{branch}:#{remoteBranch}"

    ## go back to finish branch
    
    result = shell.exec "git checkout #{finishBranch}"




    done()