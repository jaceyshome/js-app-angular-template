module.exports = (grunt)->
  shell = require 'shelljs'
  chalk = require 'chalk'
  minimatch = require 'minimatch'

  grunt.registerMultiTask 'checkunmerged', 'Check git for unmerged branches', ->
    done = @async()
    options = @options()
    result = shell.exec 'git branch -a --no-merged', {silent:true}
    exclude = options.exclude
    patterns = []
    for item in exclude
      patterns.push new RegExp(item)
    branches = []
    if result.output.length > 0
      splitBranches = result.output.split "\n"
      for branch in splitBranches
        branch = branch.trim()
        found = false
        if patterns.length > 0
          for pattern in patterns
            if branch.match pattern
              found = true
        unless found or branch is ""
          branches.push branch
    if branches.length > 0
      grunt.log.writeln "#{chalk.red('Warning: unmerged branches found')}"
      for branch in branches
        grunt.log.writeln "#{chalk.red(branch)}"
      grunt.fail.warn("Quitting build due to unmerged branches, either merge or add the branches to exceptions config")
    else
      grunt.log.writeln "#{chalk.green('No unmerged branches')}"
    done()