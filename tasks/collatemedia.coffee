module.exports = (grunt)->
  fs = require 'fs-extra'
  chalk = require 'chalk'
  md5 = require 'md5'
  http = require 'http'
  dest = null
  options = null
  extensions = [".mp4", ".webm", ".ogg", ".flv"]

  readLines = (input, path, callback)->
    str = ""
    input.on 'data', (data)->
      str += data
    input.on 'end', ()->
        processJson str, path, callback

  processJson = (str, path, callback)->
    json = JSON.parse str
    fileMap = {}
    regexes = []
    if options.extensions?
      extensions = options.extensions
    for extension in extensions
      regexes.push "^.*\.#{extension}"
    fileRegex = regexes.join "|"
    if regexes.length>0
      parseObject = (obj)->
        for k, v of obj
          if (typeof v) is "string"
            if v.search(fileRegex) isnt -1 and v.search("^(http|https|ftp)\://") isnt -1
              extension = v.substring v.lastIndexOf "."
              hash = md5 v
              newFile = hash+extension
              obj[k] = options.path+newFile
              fileMap[newFile] = v
          else if (typeof v) is "object"
            parseObject v
      parseObject json
      fs.writeFileSync path, JSON.stringify(json, null, '  ')
    downloadFiles fileMap, callback

  downloadFiles = (fileMap, callback)->
    locals = []
    remotes = []
    tempfiles = []
    for k, v of fileMap
      locals.push k
      remotes.push v
    if locals.length>0
      fs.mkdir "tmp", ->
        fs.mkdir "tmp/media", ->
          doFile = ()->
            local = locals.pop()
            remote = remotes.pop()
            tempfiles.push local
            local = "tmp/media/"+local
            fs.exists local, (exists)->
              if exists
                console.log "#{remote} exists in cache"
                next()
              else
                file = fs.createWriteStream local
                grunt.log.writeln "downloading #{remote}"
                request = http.get remote, (response)->
                  response.pipe(file)
                  size = response.headers["content-length"]
                  current = 0
                  dots = ""
                  response.on "data", (chunk)->
                    current += chunk.length
                    dotCount = 10*current/size
                    if dots.length < dotCount
                      dots += "."
                      grunt.log.write(".")
                  file.on "finish", ->
                    grunt.log.write("\n")
                    file.close()
                    next()
          next = ()->
            if locals.length>0
              doFile()
            else
              fs.mkdir dest, ->
                copyFromTemp tempfiles, callback
          doFile()
    else
      callback()

  copyFromTemp = (tempfiles, callback)->
    doCopy = ()->
      local = tempfiles.pop()
      temp = "tmp/media/"+local
      vDest = dest+"/"+local
      fs.copy temp, vDest, ->
        grunt.log.writeln "File #{chalk.cyan(vDest)} created."
        if tempfiles.length is 0
          callback()
        else
          doCopy()
    doCopy()

  grunt.registerMultiTask 'collatemedia', 'Download media files referenced in a json and change links', ->
    done = @async()
    options = @options()
    paths = []
    tally = 0
    @files.forEach (file)->
      dest = file.orig.dest
      file.src.forEach (path)->
        if grunt.file.exists path
          tally++
          paths.push path
    callback = (current_file, current_path)->
      tally--
      if tally is 0
        done()
    if paths.length>0
      for path in paths
        input = fs.createReadStream path
        readLines input, path, callback
    else
      grunt.log.writeln "No json files to process."
      done()