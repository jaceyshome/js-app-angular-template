define [
  'angular'
  ], ->
  module = angular.module 'common.text.directive', []
  module.directive 'tntText', ($compile, Text) ->
    restrict:"A"
    scope:
      tntText:"="
      removeHtmlTags: "="
    link:($scope, element, attrs) ->
      $scope.$watch "tntText", (val)->
        if val? and Text.texts
          if Text.texts[val]
            content = Text.texts[val]
            if ($scope.removeHtmlTags)
              content = $(content).text()
            element.html content
            $compile(element.contents())($scope)
          else
            console.log "Error: No text with id '" + val + "'"
            element.html val