define [
  'angular'
  'common/worldmap/directive'
  'common/worldmap/service'
], ->
  module = angular.module 'common.worldmap', [
    'templates'
    'common.worldmap.service'
    'common.worldmap.directive'
  ]
