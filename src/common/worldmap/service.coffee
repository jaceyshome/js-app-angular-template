define [
  'angular'
], (angular)->
  module = angular.module 'common.worldmap.service', []
  module.factory 'WorldMapService', ($http,$q)->
    #-----------------------------------------------------private variables

    #------------------------------------------------------public variables
    service = {}
    service.data = null
    #-----------------------------------------------------private function

    #----------------------------------------------------- public functions
    service.init = (callback)->
      deferred = $q.defer()
      if service.data
        if callback then callback()
      else
        $http.get('assets/data/worldmap_settings.json')
        .success (result, status) ->
          service.data = result
          deferred.resolve result
        if callback
          deferred.promise.then callback
        deferred.promise

    service