define [
  'angular'
  'angular_ui_router'
  'templates'
  'app/states/home/home-ctrl'
], ->
  module = angular.module 'app', [
    'templates'
    'ui.router'
    'app.states.home'
  ]

  module.config ($locationProvider, $urlRouterProvider)->
    $locationProvider.html5Mode(true)
    $urlRouterProvider.otherwise('/home')

  module.controller 'MainCtrl', ($scope) ->
    $scope.ready = true

  module