module.exports =
    dev:
      files:
        "bin/assets/css/style.css": "src/main.less"
      options:
        sourceMap:false

    deploy:
      files:
        "bin/assets/css/style.css": "src/main.less"
      options:
        sourceMap:false
        cleancss:true
        compress:true