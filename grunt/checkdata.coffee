module.exports =
    dev:
      options:
        parameters:[
          {
            value:"<%= projectName %>"
            required:true
            fatal:true
            pattern:"^((?!projectName).)*$"
            error:"Project name in package.json must not be default"
          }
          {
            value:"<%= projectChannel %>"
            required:true
            fatal:true
            pattern:"^((?!projectChannel).)*$"
            error:"gruntfile.coffee PROJECT_CHANNEL must not be default"
          }
        ]
    s3:
      options:
        parameters:[
          {
            value:"<%= s3Directory %>"
            required:true
            fatal:true
            pattern:"^((?!clientname\\/projectname).)*$"
            error:"gruntfile.coffee S3_DIRECTORY must not be default"
          }
        ]