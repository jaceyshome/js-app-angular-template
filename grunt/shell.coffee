module.exports =
    protractor_webdriver_manager_update:
      options:
        stdout: true
      command: "node "+ require('path').resolve(__dirname, 'node_modules', 'protractor', 'bin', 'webdriver-manager') + ' update'