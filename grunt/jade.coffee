module.exports =
    dev:
      expand:true
      cwd:"src"
      src:["**/*.jade"]
      dest:"templates/"
      ext:".html"
      options:
        pretty:true
    deploy:
      expand:true
      cwd:"src"
      src:["**/*.jade"]
      dest:"templates/"
      ext:".html"
      options:
        pretty:false
        data:
          deploy:true