module.exports =
    main:
      options:
        baseUrl: "bin/assets/js/app"
        mainConfigFile: "bin/assets/js/app/main.js"
        name: "main"
        wrap: true
        optimize : "uglify2"
        out: "bin/assets/js/deploy/main.js"
        include: ['../lib/almond/almond.js']
        insertRequire: ['main']