module.exports =
    less:
      files:"**/*.less"
      tasks:["buildLess"]
      options:
        spawn:true
        interrupt:true
        #livereload: true
    coffee:
      files:"**/*.coffee"
      tasks:["watchCoffee"]
      options:
        spawn:true
        interrupt:true
        #livereload: true
    jade:
      files:"**/*.jade"
      tasks:["buildJade"]
      options:
        interrupt:true
        #livereload: true
    yaml:
      files:"**/*.yml"
      tasks:["buildYaml"]
      options:
        interrupt:true
        #livereload: true
    texts:
      files:"src/**/*.md"
      tasks:["buildText"]
      options:
        interrupt:true
        #livereload: true
    bin:
      files:"bin/**/*"
      options:
        interrupt:true
        livereload: true