module.exports =
    options:
      key: 'AKIAIY3C3IUK7NLBSO3A'
      secret: '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'
      bucket: 'staging.2and2.com.au'
      access: 'public-read'
      region: 'ap-southeast-2'
    staging:
      upload:[
        {
          src: 'bin/**/*'
          dest:'<%= s3Directory %>/<%= dateStamp %>/'
          #dest:'test/'
          rel:'bin'
        }
      ]
    scorm:
      upload:[
        {
          src: 'scorm.zip'
          dest:'<%= s3Directory %>/scorm_<%= dateStamp %>.zip'
        }
      ]
    package:
      upload:[
        {
          src: 'package.zip'
          dest:'<%= s3Directory %>/package_<%= dateStamp %>.zip'
        }
      ]