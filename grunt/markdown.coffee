module.exports =
      dev:
        expand:true
        src:"texts/*.md"
        dest:"texts/"
        ext:".html"
        options:
          markdownOptions:
            highlight: "manual"
            breaks: true