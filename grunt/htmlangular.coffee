module.exports =
    templates:
      expand:true
      src:["templates/**/*.html"]
      options:
        tmplext:".html"
        reportpath: 'reports/html/html-angular-validate-templates-report.json'
    index:
      expand:true
      src:['<%= rootFile %>']
      options:
        reportpath: 'reports/html/html-angular-validate-index-report.json'
    options:
      angular:false