module.exports =
    options:
      baseUrl: "bin/assets/js/app/fix"
      mainConfigFile: "bin/assets/js/app/fix/ie8fix.js"
      name: "ie8fix"
      wrap: true
      optimize : "uglify2"
      out: "bin/assets/js/deploy/ie8fix.js"
      include: ['../../lib/almond/almond.js']
      insertRequire: ['ie8fix']