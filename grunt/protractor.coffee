module.exports =
    options:
      keepAlive: true
      noColor: false
      configFile: "node_modules/protractor/referenceConf.js"
    dev:
      options:
        args:
          specs:
            ["e2e/**/<%= spec %>-spec.coffee"]