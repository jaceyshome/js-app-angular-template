module.exports =
    dev:
      expand:true
      cwd:"tmp/bin"
      src: ['**/*.*']
      filter: 'isFile'
      options:
        version: '1.2'
        path:"tmp/bin/"