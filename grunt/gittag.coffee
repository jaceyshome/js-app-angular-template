module.exports =
  staging:
    options:
      tag: "staging_<%= tagDateStamp %>"
  scorm:
    options:
      tag: "scorm_<%= tagDateStamp %>"
  package:
    options:
      tag: "package_<%= tagDateStamp %>"
  gitstage:
    options:
      tag: "staging_<%= tagDateStamp %>"