module.exports =
    app:["bin/assets/js/"]
    templates:["templates"]
    reports:["reports"]
    texts:["texts"]
    export:["tmp/bin/assets/js/lib", "tmp/bin/assets/js/app"]
    tmp:["tmp"]
    options:
      force:true