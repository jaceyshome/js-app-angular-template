module.exports =
  less:
    options:
      title: 'less complete'
      message: 'less compiler finished'
  coffee:
    options:
      title: 'coffee complete'
      message: 'coffeescript compiler finished'
  jade:
    options:
      title: 'jade complete'
      message: 'jade compiler and html templates finished'
  text:
    options:
      title: 'text complete'
      message: 'text compiler finished'
  yaml:
    options:
      title: 'yaml complete'
      message: 'yaml compiler finished'
