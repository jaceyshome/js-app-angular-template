module.exports =
      src: ['src/main.less']
      options:
        imports: ['src/**/*.less']
        csslint:
          "unqualified-attributes": false
          "adjoining-classes": false
          "qualified-headings": false
          "unique-headings": false
          "ids": false
          "overqualified-elements": false