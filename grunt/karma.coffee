module.exports =
    dev:
      options:
        #baseUrl:"src"
        #basePath: "./"
        frameworks: [
          "mocha"
          "requirejs"
        ]
        files: [
          {pattern:"src/main-test.coffee", included:true}
          {pattern:"bin/assets/js/app/**/*.js", included:false}
          {pattern:"bin/assets/js/lib/**/*.js", included:false}
          {pattern:"bin/assets/data/**/*", served: true, included: false}
          # {pattern:"bin/assets/js/lib/moment/moment.js", included:false}
          # {pattern:"bin/assets/js/app/**/*.js", included:false}
        ]
        exclude: [
          'bin/assets/js/app/main.js'
        ]
        reporters: ["progress"]
        port: 9876
        colors: true
        autoWatch: true
        browsers: [
          #"Chrome"
          "PhantomJS"
        ]
        captureTimeout: 60000
        #singleRun: true