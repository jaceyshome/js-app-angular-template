module.exports =
    templates:
      expand:true
      cwd:"templates"
      src:"**/*.html"
      dest:"reports/wcag/"
      ext:"-report.txt"
      options:
        accessibilityLevel: 'WCAG2AA'
        ignore : [
          'WCAG2AA.Principle2.Guideline2_4.2_4_2.H25.1.NoTitleEl'
          'WCAG2AA.Principle3.Guideline3_1.3_1_1.H57.2'
        ]
    index:
      expand:true
      src:'<%= rootFile %>'
      dest:"reports/wcag/"
      ext:"-report.txt"
      options:
        accessibilityLevel: 'WCAG2AA'