module.exports =
    scorm:
      expand:true
      cwd:"tmp/bin"
      src:"**"
      dest:"./"
      options:
        archive: 'scorm.zip'
        pretty: true
    package:
      expand:true
      cwd:"tmp/bin"
      src:"**"
      dest:"./"
      options:
        archive: 'package.zip'
        pretty: true