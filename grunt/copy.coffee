module.exports =
    export:
      expand:true
      src:"bin/**/*"
      dest:"tmp/"
      filter:"isFile"
    exportExceptions: # recopy any files that are needed from js/lib that will be deleted
      files:[
        {
          src:"bin/assets/js/lib/video-js/video-js.swf"
          dest:"tmp/"
        },
        {
          src:"bin/assets/js/lib/scorm-api-wrapper/src/JavaScript/SCORM_API_wrapper.js"
          dest: "tmp/"
        }
      ]
    lib:
      files:[
        {
          src:"bower_components/video.js/dist/video-js/video-js.swf"
          dest:"bin/assets/js/lib/video.js/dist/video-js/video-js.swf"
        },
        {
          src:"bower_components/requirejs/require.js"
          dest:"bin/assets/js/lib/require.js"
        },
        {
          src:"bower_components/scorm-api-wrapper/src/JavaScript/SCORM_API_wrapper.js"
          dest: "bin/assets/js/lib/scorm-api-wrapper/src/JavaScript/SCORM_API_wrapper.js"
        },
        {
          src:"src/data/black.mp4"
          dest: "bin/assets/data/black.mp4"
        }
      ]