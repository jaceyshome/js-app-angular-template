module.exports =
  staging:
    options:
      branch:"staging"
      mergeBranch:"dev"
      remote:"staging"
      remoteBranch:"<%= tagDateStamp %>"
      autoAddServer:true
      remoteAddress:"ssh://ec2-user@<%= projectName %>.staging.2and2.com.au/home/ec2-user/staging.2and2.com.au.git"
      finishBranch:"dev"