module.exports =
    watch:
      tasks: ["watch:less", "watch:coffee", "watch:jade", "watch:yaml", "watch:texts", "watch:bin"]
      options:
        logConcurrentOutput: true
    test:
      tasks: ["watch:less", "watch:coffee", "watch:jade", "watch:yaml", "watch:texts", "karma:dev"]
      options:
        logConcurrentOutput: true