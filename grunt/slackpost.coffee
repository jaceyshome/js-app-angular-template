module.exports =
    staging:
      options:
        text:'<%= projectName %> staging build by <%= username %> staging.2and2.com.au/<%= s3Directory %>/<%= dateStamp %>/<%= launchFile %>'
        channels:[
          "builds"
          "<%= projectChannel %>"
        ]
    scorm:
      options:
        text:'<%= projectName %> scorm package by <%= username %> staging.2and2.com.au/<%= s3Directory %>/scorm_<%= dateStamp %>.zip'
        channels:[
          "builds"
          "<%= projectChannel %>"
        ]
    package:
      options:
        text:'<%= projectName %> deployment package by <%= username %> staging.2and2.com.au/<%= s3Directory %>/package_<%= dateStamp %>.zip'
        channels:[
          "builds"
          "<%= projectChannel %>"
        ]
    gitstage:
      options:
        text:'<%= projectName %> staging build by <%= username %> <%= projectName %>.staging.2and2.com.au/<%= tagDateStamp %>/'
        channels:[
          "builds"
          "<%= projectChannel %>"
        ]