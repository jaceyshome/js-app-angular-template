shell = require 'shelljs'
moment = require 'moment'
packageJson = require './package.json'
module.exports = (grunt)->
  #variables
  ROOT_FILE = "bin/index.html"
  LAUNCH_FILE = "index.html"
  #you can pass a specifc e2e spec to run with grunt e2e --spec init otherwise this runs all specs
  spec = grunt.option('spec');
  spec = "*" unless spec?
  dateStamp = moment().format('D_M_YYYY_hh:mm:ss')
  projectName = packageJson.name
  tagDateStamp = moment().format('D_M_YYYY_hh-mm-ss') #different date format supported by git tag
  result = shell.exec 'git config user.name', {silent:true}
  username = result.output
  #load config files
  require('load-grunt-config') grunt,
    data:
      rootFile:ROOT_FILE
      launchFile:LAUNCH_FILE
      spec:spec
      dateStamp:dateStamp
      projectName:projectName
      username:username
      tagDateStamp:tagDateStamp

  #load plugins
  grunt.loadTasks( 'tasks' )